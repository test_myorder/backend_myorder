import { ObjectId } from 'bson'

export interface IGenerateLinkSchema {
    _id: ObjectId
    mainLink: string
    shortLink: string
    shortId: string
}