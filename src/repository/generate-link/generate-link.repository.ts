import { Db } from 'mongodb'
import { IGenerateLinkSchema } from './generate-link.schema'
import { defaultIfEmpty, from, map, Observable } from 'rxjs'
import { IGenerateLinkModel } from '../../domain/generate-link/interface/model.interface'
import { IGenerateLinkRepository } from '../../domain/generate-link/interface/repository.interface'
import { IRepositoryMapping, MongoCollectionNameEnum } from '../../common/interface/repository.interface'
import { MongoRepository } from '../../common/mongo-repository'

export class GenerateLinkRepository extends MongoRepository<IGenerateLinkModel> implements IGenerateLinkRepository {

    constructor(
        db: Db,
        mapping: IRepositoryMapping<IGenerateLinkModel, IGenerateLinkSchema>,
    ) {
        super(db.collection(MongoCollectionNameEnum.GENERATE_LINK), mapping)
    }

    public getByMainLink(mainLink: string): Observable<IGenerateLinkModel> {
        const promise = this.findOne({mainLink: mainLink})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((generateLinkModel: IGenerateLinkModel) => {
                return generateLinkModel
            }),
        )
    }

    public getByShortLink(shortId: string): Observable<IGenerateLinkModel> {
        const promise = this.findOne({shortId: shortId})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((generateLinkModel: IGenerateLinkModel) => {
                return generateLinkModel
            }),
        )
    }
}