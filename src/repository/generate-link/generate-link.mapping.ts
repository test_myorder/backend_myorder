import { IGenerateLinkSchema } from './generate-link.schema'
import { plainToClass } from 'class-transformer'
import { ObjectId } from 'mongodb'
import * as _ from 'lodash'
import { IGenerateLinkModel } from '../../domain/generate-link/interface/model.interface'
import { GenerateLinkModel } from '../../domain/generate-link/generate-link.model'
import { IRepositoryMapping } from '../../common/interface/repository.interface'

export class GenerateLinkRepositoryMapping implements IRepositoryMapping<IGenerateLinkModel, IGenerateLinkSchema> {
    public deserialize(schema: IGenerateLinkSchema): IGenerateLinkModel {
        return plainToClass(GenerateLinkModel, {
            _id: schema._id.toHexString(),
            _mainLink: schema.mainLink,
            _shortLink: schema.shortLink,
            _shortId: schema.shortId
        })
    }

    public serialize(model: IGenerateLinkModel): IGenerateLinkSchema {
        return {
            _id: !_.isNil(model.getId()) ? new ObjectId(model.getId()) : new ObjectId(),
            mainLink: model.getMainLink(),
            shortLink: model.getShortLink(),
            shortId: model.getShortId(),
        }
    }

}