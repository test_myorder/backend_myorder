import { IEntity } from './interface/entity.interface'
import * as _ from 'lodash'

export abstract class Entity implements IEntity {
    private _id: string
    private _createdAt: Date
    private _createdBy: string
    private _updatedAt: Date
    private _updatedBy: string

    public getId(): string {
        return this._id
    }

    public setId(id: string): void {
        this._id = id
    }

    public getCreatedAt(): Date {
        return !_.isNil(this._createdAt) ? new Date(this._createdAt) : null
    }

    public setCreatedAt(date: Date) {
        this._createdAt = !_.isNil(date) ? date : new Date()
    }

    public getCreatedBy(): string {
        return this._createdBy
    }

    public setCreatedBy(createdBy: string) {
        this._createdBy = createdBy
    }

    public getUpdatedAt(): Date {
        return !_.isNil(this._updatedAt) ? new Date(this._updatedAt) : null
    }

    public setUpdatedAt(date: Date) {
        this._updatedAt = !_.isNil(date) ? date : null
    }

    public getUpdatedBy(): string {
        return this._updatedBy
    }

    public setUpdatedBy(updatedBy: string) {
        this._updatedBy = updatedBy
    }
}
