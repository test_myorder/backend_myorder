export interface IConfig {
    env: string
    application: {
        host: string
        port: number
    }
    mongodb: IMongoDB
}

export interface IMongoDB {
    servers: string
    port: number
    dbName: string
    username?: string
    password?: string
    authSource?: string
    replicaSetName?: string
    appName?: string
}
