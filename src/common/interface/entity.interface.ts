export interface IEntity {
    getId(): string

    setId(id: string): void

    // getCreatedAt(): Date
    //
    // setCreatedAt(date: Date): void
    //
    // getCreatedBy(): string
    //
    // setCreatedBy(createdBy: string): void
    //
    // getUpdatedAt(): Date
    //
    // setUpdatedAt(date: Date): void
    //
    // getUpdatedBy(): string
    //
    // setUpdatedBy(updatedBy: string): void
}
