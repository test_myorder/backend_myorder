import { NestFactory } from '@nestjs/core'
import { MainModule } from './module/main.module'
import {
    Logger,
    ValidationPipe,
} from '@nestjs/common'
import * as _ from 'lodash'
import {
    DocumentBuilder,
    SwaggerModule,
} from '@nestjs/swagger'

async function bootstrap() {
    const port = _.toNumber(process.env.PORT) || 3000
    const app = await NestFactory.create(MainModule, {
        cors: true,
    })

    app.useGlobalPipes(
        new ValidationPipe({
            transform: true,
        }),
    )

    if (!_.isEqual(process.env.ENV, 'production')) {
        const config = new DocumentBuilder()
            .setTitle('API Document')
            .setDescription('API Endpoint List')
            .setVersion('1.0')
            .build()
        const document = SwaggerModule.createDocument(app, config)
        SwaggerModule.setup('document', app, document)
    }

    await app.listen(port)
    return port
}

bootstrap().then(port => {
    Logger.log(`application started on port ${port}`)
})
