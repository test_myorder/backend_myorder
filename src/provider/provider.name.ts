enum CommonProviderNames {
    CONFIG = 'common-config-provider',
    MONGO_CONNECTION = 'mongo-connection-provider',
}


enum GenerateLinkProviderNames {
    GENERATE_LINK_REPOSITORY = 'generate-link-repository-provider',
    GENERATE_LINK_REPOSITORY_MAPPING = 'generate-link-repository-mapping-provider',
    GENERATE_LINK_SERVICE = 'generate-link-service-provider'
}

export const providerNames = Object.assign({},
    CommonProviderNames,
    GenerateLinkProviderNames,
)
