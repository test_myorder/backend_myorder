import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { IConfig } from '../common/interface/config.interface'
import { Db, MongoClient } from 'mongodb'
import * as _ from 'lodash'

export const configProvider: Provider = {
    provide: providerNames.CONFIG,
    useFactory: () => {
        const processEnv = process.env
        const configFile: IConfig = {
            env: processEnv.ENV,
            application: {
                host: processEnv.HOST,
                port: _.toNumber(processEnv.PORT),
            },
            mongodb: {
                servers: processEnv.MONGO_HOST,
                port: _.toNumber(processEnv.MONGO_PORT),
                dbName: processEnv.MONGO_DB_NAME,
                username: processEnv.MONGO_DB_USERNAME,
                password: processEnv.MONGO_DB_PASSWORD,
                appName: processEnv.MONGO_DB_APP_NAME,
                replicaSetName: processEnv.MONGO_DB_REPLICA_NAME,
            },
        }
        return configFile
    },
}

export const mongoConnection: Provider = {
    provide: providerNames.MONGO_CONNECTION,
    useFactory: async (config: IConfig): Promise<Db> => {
        if (config && config.mongodb) {
            const mongoConfig = config.mongodb
            const servers = process.env.MONGO_URL || mongoConfig.servers
            let auth = ''
            if (mongoConfig.username || mongoConfig.password) {
                auth = `${mongoConfig.username}:${mongoConfig.password}@`
            }
            let url: string =
                'mongodb://' +
                auth +
                servers
                    .split(',')
                    .map((server: string) => {
                        return server + ':' + mongoConfig.port
                    })
                    .toString() +
                '/' +
                mongoConfig.dbName
            return await MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true,
            }).then((client: MongoClient) => client.db(mongoConfig.dbName))
        }
        return Promise.reject('Cannot connect MongoDB')
    },
    inject: [providerNames.CONFIG],
}