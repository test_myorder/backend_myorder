import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { Db } from 'mongodb'
import { IRepositoryMapping } from '../common/interface/repository.interface'
import { IConfig } from '../common/interface/config.interface'
import { GenerateLinkService } from '../domain/generate-link/generate-link.service'
import { GenerateLinkRepository } from '../repository/generate-link/generate-link.repository'
import { GenerateLinkRepositoryMapping } from '../repository/generate-link/generate-link.mapping'
import { IGenerateLinkSchema } from '../repository/generate-link/generate-link.schema'
import { IGenerateLinkRepository } from '../domain/generate-link/interface/repository.interface'
import { IGenerateLinkModel } from '../domain/generate-link/interface/model.interface'

export const generateLinkRepositoryProviders: Provider[] = [
    {
        provide: providerNames.GENERATE_LINK_REPOSITORY_MAPPING,
        useClass: GenerateLinkRepositoryMapping,
    },
    {
        provide: providerNames.GENERATE_LINK_REPOSITORY,
        inject: [
            providerNames.MONGO_CONNECTION,
            providerNames.GENERATE_LINK_REPOSITORY_MAPPING,
        ],
        useFactory: (
            db: Db,
            mapping: IRepositoryMapping<IGenerateLinkModel, IGenerateLinkSchema>,
        ) => {
            return new GenerateLinkRepository(db, mapping)
        },
    },
]

export const generateLinkServiceProviders: Provider = {
    provide: providerNames.GENERATE_LINK_SERVICE,
    inject: [
        providerNames.CONFIG,
        providerNames.GENERATE_LINK_REPOSITORY,
    ],
    useFactory: (
        config: IConfig,
        generateLinkRepository: IGenerateLinkRepository,
    ) => {
        return new GenerateLinkService(
            config,
            generateLinkRepository,
        )
    },
}