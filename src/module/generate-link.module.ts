import { Module } from '@nestjs/common'
import { GenerateLinkController } from '../controller/generate-link/generate-link.controller'
import { generateLinkRepositoryProviders, generateLinkServiceProviders } from '../provider/generate-link.provider'

@Module({
    controllers: [
        GenerateLinkController,
    ],
    providers: [
        generateLinkServiceProviders,
        ...generateLinkRepositoryProviders,
    ],
    imports: [],
    exports: [
        generateLinkServiceProviders,
        ...generateLinkRepositoryProviders,
    ],
})
export class GenerateLinkModule {
}