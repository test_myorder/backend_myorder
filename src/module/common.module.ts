import { Global, Module, Provider } from '@nestjs/common'
import { configProvider, mongoConnection } from '../provider/common.provider'

const globalProviders: Provider[] = [
    configProvider,
    mongoConnection,
]

@Global()
@Module({
    controllers: [],
    providers: globalProviders,
    exports: globalProviders,
})

export class CommonModule {}
