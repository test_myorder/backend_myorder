import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { CommonModule } from './common.module'
import { GenerateLinkModule } from './generate-link.module'

@Module({
    imports: [
        ConfigModule.forRoot(),
        CommonModule,
        GenerateLinkModule,
    ]
})

export class MainModule {}
