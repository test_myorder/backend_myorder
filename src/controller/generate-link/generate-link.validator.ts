import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsString, IsUrl } from 'class-validator'
import { IGenerateValidator } from '../../domain/generate-link/interface/validator.interface'

export class GenerateValidator implements IGenerateValidator {
    @ApiProperty({
        name: 'mainLink',
        required: true,
        type: String,
    })
    @IsUrl()
    @IsNotEmpty()
    @IsString()
    private mainLink: string

    public getMainLink(): string {
        return this.mainLink
    }
}