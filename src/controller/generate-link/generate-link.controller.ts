import { ApiOperation, ApiTags } from '@nestjs/swagger'
import { Body, Controller, Get, Inject, Param, Post } from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { IGenerateLinkService } from '../../domain/generate-link/interface/service.interface'
import { GenerateValidator } from './generate-link.validator'

@ApiTags('Generate-link')
@Controller('/generate-link')
export class GenerateLinkController {
    constructor(
        @Inject(providerNames.GENERATE_LINK_SERVICE)
        private readonly _generateLinkService: IGenerateLinkService,
    ) {}

    @ApiOperation({
        summary: 'generate link',
    })
    @Post('/')
    public generate(
        @Body() body: GenerateValidator,
    ) {
        return this._generateLinkService.generate(body)
    }

    @ApiOperation({
        summary: 'get all list',
    })
    @Get('/')
    public getAll() {
        return this._generateLinkService.findAll()
    }

    @ApiOperation({
        summary: 'get by short id',
    })
    @Get('/:shortId')
    public get(
        @Param('shortId') shortId: string,
    ) {
        return this._generateLinkService.getByShortLink(shortId)
    }
}