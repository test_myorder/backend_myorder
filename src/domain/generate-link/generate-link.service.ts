import { IGenerateLinkService } from './interface/service.interface'
import { defaultIfEmpty, forkJoin, mergeMap, Observable, of } from 'rxjs'
import { IGenerateLinkRepository } from './interface/repository.interface'
import { GenerateLinkBuilder } from './generate-link.builder'
import { IGenerateValidator } from './interface/validator.interface'
import { IGenerateLinkModel } from './interface/model.interface'
import { IConfig } from '../../common/interface/config.interface'
import * as _ from 'lodash'
import * as shortid from 'shortid'
import { map } from 'rxjs/operators'
import { NotFoundException } from '@nestjs/common'

export class GenerateLinkService implements IGenerateLinkService {
    constructor(
        private readonly _config: IConfig,
        private readonly _generateLinkRepository: IGenerateLinkRepository,
    ) {
    }

    public generate(data: IGenerateValidator): Observable<object> {
        return this._generateLinkRepository.getByMainLink(data.getMainLink()).pipe(
            defaultIfEmpty(null),
            mergeMap((generateLink: IGenerateLinkModel) => {
                if(!_.isNil(generateLink)){
                    const oldLink: object = {
                        mainLink: generateLink.getMainLink(),
                        shortLink: generateLink.getShortLink(),
                        shortId: generateLink.getShortId(),
                    }
                    return of(oldLink)
                }
                const newGenerateLink = new GenerateLinkBuilder()
                const urlId: string = shortid.generate()
                const hostLink: string = this._config.application.host+':3501'
                const newShortLink: string = hostLink+'/'+urlId
                const newLink: object = {mainLink: data.getMainLink(), shortLink: newShortLink, shortId: urlId}
                newGenerateLink.setMainLink(data.getMainLink())
                newGenerateLink.setShortLink(newShortLink)
                newGenerateLink.setShortId(urlId)
                return forkJoin([
                    this._generateLinkRepository.insert(newGenerateLink.build()),
                    [newLink]
                ]).pipe(
                    map((res: any[]) =>{
                        return res[1]
                    })
                )
            })
        )
    }

    public getByShortLink(shortId: string): Observable<object> {
        return this._generateLinkRepository.getByShortLink(shortId).pipe(
            defaultIfEmpty(null),
            mergeMap((generateLink: IGenerateLinkModel) => {
                if(_.isNil(generateLink)){
                    throw new NotFoundException('Not Found Main Link')
                }
                const newLink: object = {mainLink: generateLink.getMainLink(), shortLink: generateLink.getShortLink()}
                return of(newLink)
            })
        )
    }

    public findAll(): Observable<IGenerateLinkModel> {
        return this._generateLinkRepository.find()
    }
}