import { IGenerateLinkModel } from './interface/model.interface'
import { GenerateLinkModel } from './generate-link.model'

export class GenerateLinkBuilder {
    private readonly _model: IGenerateLinkModel

    constructor() {
        this._model = new GenerateLinkModel()
    }

    public build(): IGenerateLinkModel {
        return this._model
    }

    public setMainLink(mainLink: string) {
        this._model.setMainLink(mainLink)
        return this
    }

    public setShortLink(shortLink: string) {
        this._model.setShortLink(shortLink)
        return this
    }

    public setShortId(shortId: string){
        this._model.setShortId(shortId)
        return this
    }
}