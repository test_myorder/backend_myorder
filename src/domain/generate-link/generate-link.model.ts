import { IGenerateLinkModel } from './interface/model.interface'
import { Entity } from '../../common/entity'

export class GenerateLinkModel extends Entity implements IGenerateLinkModel {
    private _mainLink: string
    private _shortLink: string
    private _shortId: string

    public getMainLink(): string {
        return this._mainLink
    }

    public setMainLink(mainLink: string): void {
        this._mainLink = mainLink
    }

    public getShortLink(): string {
        return this._shortLink
    }

    public setShortLink(shortLink: string): void {
        this._shortLink = shortLink
    }

    public getShortId(): string {
        return this._shortId
    }

    public setShortId(shortId: string): void {
        this._shortId = shortId
    }

}