import { IGenerateLinkModel } from './model.interface'
import { Observable } from 'rxjs'
import { IRepository } from '../../../common/interface/repository.interface'

export interface IGenerateLinkRepository extends IRepository<IGenerateLinkModel> {
    getByMainLink(id: string): Observable<IGenerateLinkModel>

    getByShortLink(shortId: string): Observable<IGenerateLinkModel>
}