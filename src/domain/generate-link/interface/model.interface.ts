import { IEntity } from '../../../common/interface/entity.interface'

export interface IGenerateLinkModel extends IEntity {
    getMainLink(): string
    setMainLink(mainLink: string): void

    getShortLink(): string
    setShortLink(shortLink: string): void

    getShortId(): string
    setShortId(shortId: string): void

}