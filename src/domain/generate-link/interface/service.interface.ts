import { Observable } from 'rxjs'
import { IGenerateValidator } from './validator.interface'
import { IGenerateLinkModel } from './model.interface'

export interface IGenerateLinkService {
    generate(data: IGenerateValidator): Observable<object>

    getByShortLink(shortId: string): Observable<object>

    findAll(): Observable<IGenerateLinkModel>
}

